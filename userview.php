<html>
<head>
	<title>Infinity Loop Level Browser</title>
	<link rel="stylesheet" type="text/css" href="personal_core/userview/design.css">
    <script src="personal_core/jquery-2.2.0.min.js"></script>
    <script src="personal_core/loader.js"></script>
    <script src="personal_core/userview/core.js"></script>
	<script src="personal_core/dataController.js"></script>
	<script src="personal_core/canvasController.js"></script>
    <script src="personal_core/userview/map_controller.js"></script>
	<!-- <script src="http://www.openlayers.org/api/OpenLayers.js"></script> -->

    <script>
    var reload_delay = 5000;
    function resetBar(){
        $("div#progressbar").animate({width: "28vh"}, 150);
    };
    (function auto_reload() {
        $.post( "http://api.infinityloop.mobi/web_get_latest_level",{user_id:"1111"}, function( data ) {
            resetBar();

    		init(data);
            $("div#progressbar").animate({width:"0vh"}, reload_delay -150);
            setTimeout(auto_reload, reload_delay);
        });
    })();
    $(document).ready(sizingFixes);

    $(window).resize(sizingFixes);

    function sizingFixes(){
        if(currentMap.map !== undefined )
            google.maps.event.trigger(currentMap.map, "resize");
        var cw = $('#logo').width();
        $('#logo').css({'height':cw+'px'});
        $('.canvas_history').each(function(i, obj) {
            $(this).css({'height':$(this).width()+'px'});
        });
    };


    </script>
</head>
<body>
	<?php
		echo "
            <div class='main_box'>
                <div id='left_block'>
                    <div id='logo'></div>
                    NEWEST<br/>PUZZLES
                </div>
                <div id='right_block'>
                    <div id='history_annotation'>PREVIOUS:</div>

                </div>
                <div class='element_holder'>
                    <div id='overmap' class='curved_shadow'></div>
                    <table id='main_view'></table>
                    <div id='progressbar'></div>
                </div>
                <div id='shoe_box'></div>
            </div>
        ";
	 ?>
     <script async defer
         src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-UOg98HEnLWlCKTu3Yr5KWh6X2qWNuCM&callback=initMap"
         type="text/javascript"
     ></script>
 </body>
 </html>
