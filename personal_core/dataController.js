var dataHolder = null;
var ilLevelData = function(init_data){
    var il = {
        data : 0
    };
    il.data = stoJSON(init_data);
    var i = 0;
    for( i in il.data.levels){
        il.data.levels[i].level_string = stoJSON(il.data.levels[i].level_string);
    }
    return il.data;
};
