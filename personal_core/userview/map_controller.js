var sloadmap = true;
var currentMap = {};

var API_KEY = "AIzaSyC-UOg98HEnLWlCKTu3Yr5KWh6X2qWNuCM";

function initMap(lat, lon, mapdivid){
    var map = new google.maps.Map(document.getElementById('overmap'), {
        center: {lat: 0, lng: 0},
        zoom: 1,
        disableDefaultUI: true,
        draggable : false,
        scrollwheel : false
    });
    var allowedBounds = new google.maps.LatLngBounds(
        new google.maps.LatLng(85, -180),           // top left corner of map
        new google.maps.LatLng(-85, 180)            // bottom right corner
    );
    var k = 5.0;
    var n = allowedBounds .getNorthEast().lat() - k;
    var e = allowedBounds .getNorthEast().lng() - k;
    var s = allowedBounds .getSouthWest().lat() + k;
    var w = allowedBounds .getSouthWest().lng() + k;
    var neNew = new google.maps.LatLng( n, e );
    var swNew = new google.maps.LatLng( s, w );
    boundsNew = new google.maps.LatLngBounds( swNew, neNew );
    map .fitBounds(boundsNew);
    currentMap.map = map;
}
function markerControl(lat,lon, country){

    if(currentMap.marker === null || currentMap.marker === undefined){
        var pos = {lat: lat, lng: lon};
        var marker = new google.maps.Marker({
            position: pos,
            map: currentMap.map,
            title: 'IL_LEVEL_POS_MARKER'
        });
        marker.info = new google.maps.InfoWindow({
          content: country
        });
        marker.info.open(currentMap.map, marker);
        currentMap.marker = marker;

    }else{
        currentMap.marker.setPosition( new google.maps.LatLng( lat,lon ) );
        currentMap.marker.info.close();
        currentMap.marker.info = new google.maps.InfoWindow({
          content: country
        });
        currentMap.marker.info.open(currentMap.map, currentMap.marker);
    }
}
