var histogram_enabled = false;
var store_history = false;
var old_canvas=null;

function getMainBox(){
    return document.getElementsByClassName('element_holder')[0];
}
function createCallbackFunction(func, args){
    return function(){ func(args);};
}
function stoJSON(data){
    obj = JSON.parse(data);
    return obj;
}
function histogramAdd(el){
    if(histogram_enabled){
        var t = $("#histogram").html();
        t = el + "<br/>" + t;
        $("#histogram").html(t);
    }
}
function doneLoading(){
    var table = document.getElementById("main_view");
    var currentRow = 0;
    var currentCell = -1;
    var crow;
    var setMarker = function(id){
        $.get( "http://freegeoip.net/json/" + dataHolder.levels[id].location, function( data_two ) {
            markerControl(data_two.latitude, data_two.longitude, data_two.country_name);
        });
    };

    var toggleMap = function(id){
        var map = document.getElementsByClassName(id);
        if(current.style.visibility == 'hidden'){
            current.style.visibility = 'visible';
        }
        else{
            current.style.visibility = 'hidden';
        }

    };

    for (var i in dataHolder.levels){
        if(currentCell >= 1){
            currentRow+=1;
            currentCell = 0;
        }
        else{
            currentCell+= 1;
        }

        if(dataHolder.levels[i].location !== null && dataHolder.levels[i].location !== "" && sloadmap){
            setMarker(i);
        }
        if(old_canvas !== null){
            var history_canvas = cloneCanvas(old_canvas);
            storeHistoryCanvas(history_canvas);
        }
        var c = new singleCanvas(dataHolder.levels[i].level_string);
        if(currentCell === 0){
            crow = table.insertRow(currentRow);
        }
        ccell = crow.insertCell(currentCell);

        addElementTo(ccell, c);
    }
    if(!store_history)
        store_history = true;
}

function convertType(type){
    switch(type){
        case 0:
            return 3;
        case 1:
            return 4;
        case 2:
            return 0;
        case 3:
            return 1;
        case 4:
            return 2;
        //ALIEN
        case 8:
            return 4;
        case 9:
            return 3;
        case 10:
            return 0;
        case 11:
            return 1;
        case 12:
            return 2;
        //ELASTIC:
        case 13:
            return 4;
        case 14:
            return 3;
        case 15:
            return 0;
        case 16:
            return 1;
        case 17:
            return 2;
        //----
        default:
            return 0;
    }
}
function resetSite(){
    old_canvas = document.getElementById('level_big');
    var toremove = document.getElementById('main_view');
    var parent = toremove.parentElement;
    toremove.parentElement.removeChild(toremove);

    var fresh = document.createElement('table');
    fresh.id = 'main_view';
    var bf = 0;
    for(var i in parent.childNodes){
        if(parent.childNodes[i].id == "progressbar")
            bf = i;
    }
    parent.insertBefore(fresh, parent.childNodes[bf]);
    loadImages();
}
function init(data){
    if(dataHolder !== null && dataHolder !== []){
        var t = new ilLevelData(data);
        if( t == dataHolder ){
            return;
        }
        else{
            dataHolder = new ilLevelData(data);
            resetSite();
            return;
        }
    }
    dataHolder = new ilLevelData(data);
    loadImages();

}
function addElement(el){
    document.body.appendChild(el);
}
function addElementTo(to, el){
    to.appendChild(el);
}
