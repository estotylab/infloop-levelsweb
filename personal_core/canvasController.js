var canvasStyles = [
    "top",
    "mid",
    "bot"
];
var lastCanvasStyle = 0;

var cloneCanvas = function(old_canvas){
    var canvas;
    canvas = document.createElement('canvas');
    canvas.width = 245;
    canvas.height = 245;

    var context = canvas.getContext('2d');
    context.drawImage(old_canvas, 0, 0);

    return canvas;
};

var singleCanvas = function(data){
    var canvas;
    canvas = document.createElement('canvas');
    canvas.id     = "level_big";
    canvas.width  = 245;
    canvas.height = 245;

    var context = canvas.getContext('2d');
    var imageSize = {w: 64, h: 64};
    var i = 0;
    var highestRow = 0;
    var highestCol = 0;
    for( i in data.data){
        if(data.data[i].column > highestCol){
            highestCol = data.data[i].column;
        }
        if(data.data[i].row > highestRow){
            highestRow = data.data[i].row;
        }
    }highestRow+=1;highestCol+=1;
    var add = {x:0, y:0};
    var totalSize = {w: highestCol * 64, h: highestRow * 64}; var bsize;
    var diff = Math.abs(highestCol - highestRow);
    if( totalSize.w > totalSize.h){
        bsize = totalSize.w;
        add.y = diff*32;
    }
    else{
        bsize = totalSize.h;
        add.x = diff*32;
    }
    if(totalSize.w == totalSize.h){
        add.x = 0;
        add.y = 0;
    }

    var scale = (canvas.width / bsize);
    context.scale(scale, scale);
    for(i in data.data){
        var current = loadImageToContext(canvas, loaded_images[convertType(data.data[i].type)], context,{
                x:data.data[i].column*64 +add.x,// + (canvas.width/2 - (totalSize.w-1)/2) + canvas.width/2,
                y:data.data[i].row*64 + add.y// + (canvas.height/2 - (totalSize.h-1)/2) + canvas.height/2
            }, imageSize, data.data[i].correctRotation
        );

    }
    return canvas;
};
function loadImageToContext(canvas, img, cont, pos, size,rotation){
    var imageObj = img;
    cont.save();
    cont.translate(pos.x+size.w/2, pos.y+size.h/2);
    cont.rotate(rotation*Math.PI/180);
    cont.drawImage(imageObj, -(size.w/2), -(size.h/2), size.w, size.h);
    cont.restore();
    return imageObj;
}

function storeHistoryCanvas(hc){

    var t = document.getElementsByClassName('canvas_history');
    if(t.length === 0){
        $("#history_annotation").animate({opacity: "1"}, 500);
        hc.className = 'canvas_history';
        hc.id = 'top';
        addElementTo(document.getElementById('right_block'), hc);
        $('#top.canvas_history').hide();
        $('#top.canvas_history').slideDown('normal');
    }else{
        while(t.length >= 3){
            console.log(t.length);
            var $toremove = jQuery(document.getElementById('right_block').lastChild);
            $toremove.remove();
            // console.log()
        }
        hc.className = 'canvas_history';
        hc.id = canvasStyles[getNextCanvasStyle()];

        var parent = document.getElementById('right_block');

        var bf = 0;
        for(var i in parent.childNodes){
            if(parent.childNodes[i].id == "history_annotation")
                bf = i;
        }

        parent.insertBefore(hc, parent.childNodes[bf].nextSibling);
        // addElementTo(document.getElementById('right_block'), hc);
        $('#'+hc.id + '.canvas_history').hide();
        $('#'+hc.id + '.canvas_history').slideDown('normal');

    }
}
function getNextCanvasStyle(){
    if(lastCanvasStyle >= 2)
        lastCanvasStyle = 0;
    else
        lastCanvasStyle++;
    return lastCanvasStyle;
}
