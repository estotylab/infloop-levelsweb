var images = [
    "images/tiles/1.png",
    "images/tiles/2.png",
    "images/tiles/3.png",
    "images/tiles/4.png",
    "images/tiles/5.png"
];
var loaded_images = [

];

var loaded = false;

function loadSingleImage(link, callback){
    var imageObj = new Image();
    imageObj.onload = function() {
        loaded_images.push(this);
        callback();
    };
    imageObj.src = link;
    return imageObj;
}
function loadImages(){
    var i = -1;
    var loader = function(){
        if(i+1 < images.length && loaded === false){
            i++;
            loadSingleImage(images[i], loader);
        }else{
            loaded = true;
            doneLoading();
        }
    };
    loader();
}
