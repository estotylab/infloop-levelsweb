function getMainBox(){
    return document.getElementsByClassName('main_box')[0];
}
function createCallbackFunction(func, args){
    return function(){ func(args);};
}
function stoJSON(data){
    obj = JSON.parse(data);
    return obj;
}
function doneLoading(){
    var table = document.getElementById("main_view");
    var currentRow = 0;
    var currentCell = -1;
    var crow;
    var loadMap = function(id){
        $.get( "http://freegeoip.net/json/" + dataHolder.levels[id].location, function( data_two ) {
            initMap(data_two.latitude, data_two.longitude, id);
        });
    };
    var repositionMap = function(id){
        $.get( "http://freegeoip.net/json/" + dataHolder.levels[id].location, function( data_two ) {
            reposMap(data_two.latitude, data_two.longitude, id);
        });
    };

    var toggleMap = function(id){
        var map = document.getElementsByClassName(id);
        if(current.style.visibility == 'hidden'){
            current.style.visibility = 'visible';
        }
        else{
            current.style.visibility = 'hidden';
        }

    };

    for (var i in dataHolder.levels){
        if(currentCell >= 1){
            currentRow+=1;
            currentCell = 0;
        }
        else{
            currentCell+= 1;
        }

        var c = new singleCanvas(dataHolder.levels[i].level_string);
        if(currentCell === 0){
            crow = table.insertRow(currentRow);
        }
        ccell = crow.insertCell(currentCell);

        addElementTo(ccell, c);
        var level_id = document.createElement('div');
        level_id.id = "id_holder";
        level_id.innerHTML = dataHolder.levels[i].level_id;
        addElementTo(ccell, level_id);
        var button_holder = document.createElement('div');
        button_holder.id = "button_holder";
        addElementTo(ccell, button_holder);

        var button_a = document.createElement('BUTTON');
        button_a.innerHTML = "PUBLISH";
        addElementTo(button_holder, button_a);

        var button_b = document.createElement('BUTTON');
        button_b.innerHTML = "UNPUBLISH";
        addElementTo(button_holder, button_b);

        // var test_div = document.createElement('div');
    }
}
function convertType(type){
    // line = 4.jpg
    // arc = 5.jpg
    // cross = 1.jpg
    // RLT = 2.jpg
    // ONE WAY = 3.jpg

    switch(type){
        //NORMAL
        case 0:
            return 3;
        case 1:
            return 4;
        case 2:
            return 0;
        case 3:
            return 1;
        case 4:
            return 2;
        //ALIEN
        case 8:
            return 4;
        case 9:
            return 3;
        case 10:
            return 0;
        case 11:
            return 1;
        case 12:
            return 2;
        //ELASTIC:
        case 13:
            return 4;
        case 14:
            return 3;
        case 15:
            return 0;
        case 16:
            return 1;
        case 17:
            return 2;
        //----
        default:
            return 0;
    }
}
function resetSite(){
    var toremove = document.getElementById('main_view');
    var parent = toremove.parentElement;
    toremove.parentElement.removeChild(toremove);

    var fresh = document.createElement('table');
    fresh.id = 'main_view';
    // fresh.style.opacity = '0';
    addElementTo(parent,fresh);
    loadImages();
}
function init(data){
    if(dataHolder !== null && dataHolder !== []){
        var t = new ilLevelData(data);
        if( t == dataHolder ){
            return;
        }
        else{
            // resetSite();
            dataHolder = new ilLevelData(data);
            // setTimeout(loadImages,500);
            resetSite();
            return;
        }
    }
    dataHolder = new ilLevelData(data);
    loadImages();

}
